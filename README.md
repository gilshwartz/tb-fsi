Finally Sort Identities
=======================

This extension for Thunderbird 24+ adds a "Finally Sort Identities" entry into
the Tools menu to allow the sorting of an account's list of identities.

It was inspired by the very useful add-on Manually Sort Folders by Jonathan Protzenko.

The xpi directory has a ready to download and install .xpi file.