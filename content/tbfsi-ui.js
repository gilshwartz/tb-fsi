const Cc = Components.classes;
const Ci = Components.interfaces;

function onLoad() {
	loadAccounts();
	onAccountChanged();
}

function loadAccounts() {
	var accountsMenu = document.getElementById("accounts_menu");
	
	var accountIds = Application.prefs.get("mail.accountmanager.accounts").value.split(",");
	var localFolderServerId = Application.prefs.get("mail.accountmanager.localfoldersserver").value;

	var serverId;
	var name;
	
	for each (var accountId in accountIds) {
		try {
			serverId = Application.prefs.get("mail.account."+accountId+".server").value;
			if (serverId == localFolderServerId) {
				continue;
			}
			
			name = Application.prefs.get("mail.server."+serverId+".name").value;
			
		} catch (e) {
			continue;
		}
		
		var mi = document.createElement("menuitem");
		mi.setAttribute("label", name);
		mi.value = accountId;

		accountsMenu.appendChild(mi);
	}

	accountsMenu.parentNode.selectedIndex = 0;
}	

var selectedAccount = null;

function onAccountChanged() {
	var newAccount = document.getElementById("accounts_menu").parentNode.selectedItem.value;
	if (newAccount != selectedAccount) {
		selectedAccount = newAccount;
		loadIdentities();
	}
}

function loadIdentities() {
	var identitiesList = document.getElementById("identities_list");

	for (var i = identitiesList.children.length; i > 0; i--) {
		identitiesList.removeItemAt(0);
	}

	var identityIds = Application.prefs.get("mail.account."+selectedAccount+".identities").value.split(",");

	var name;
	var email;
	
	for each (var identityId in identityIds) {
		try {
			name = Application.prefs.get("mail.identity."+identityId+".fullName").value;
			
		} catch (e) {
			name = "";
		}
		email = Application.prefs.get("mail.identity."+identityId+".useremail").value;
		
		var li = document.createElement("listitem");
		li.setAttribute("label", name+" <"+email+">");
		li.value = identityId;

		identitiesList.appendChild(li);
	}
}	

function onIdentityUp() {
	identityMove(-1);
}

function onIdentityDown() {
	identityMove(1);
}

// direction: -1 is up and +1 is down
function identityMove(direction) {
	var identitiesList = document.getElementById("identities_list");

	var i = identitiesList.selectedIndex;
	if (i < 0) {
		return;
	}

	var item = identitiesList.getItemAtIndex(i + ((1 + direction) / 2));
	if (!item) {
		return;
	}

	var previous_item = item.previousSibling;
	if (!previous_item) {
		return;
	}

	var parent = item.parentNode;
	parent.insertBefore(parent.removeChild(item), previous_item);

    identitiesList.selectedIndex = i + direction;

	updateIdentitiesPrefs();
}

function updateIdentitiesPrefs() {
	var identitiesList = document.getElementById("identities_list");

	var identityIds = "";
	for (var i = 0; i < identitiesList.children.length; i++) {
		identityIds += identitiesList.children[i].value + ",";
	}
	
	Application.prefs.get("mail.account."+selectedAccount+".identities").value = identityIds.slice(0, -1);
}

function onRestart() {
	var mainWindow = Cc['@mozilla.org/appshell/window-mediator;1']
		.getService(Ci.nsIWindowMediator)
		.getMostRecentWindow("mail:3pane");

	mainWindow.setTimeout(function () {
		mainWindow.Application.restart();
	}, 1000);
	
	window.close();
}

function onClose() {
	window.close();
}
